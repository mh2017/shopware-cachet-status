# Shopware 6 & Cachet

This small shellscript will help you monitor your shopware 6 store.


# Setup

 1. First install https://github.com/CachetHQ/Cachet on your server or
    locally
2. Setup your config
3. Execute status.sh with `./status.sh or bash status.sh`
4. Check Dashboard for data

# Config

CACHET_API_VERSION="v1"  
CACHET_URL="http://cachet.local/api/${CACHET_API_VERSION}/"  
CACHET_KEY="XXXXXXXXXXXXXXXXXXXXXXXXXX"   
CACHET_RESPONSE_TIME_METRIC_ID=1  
CACHET_SHOP_COMPONENT_ID=1  
CACHET_STORE_API_COMPONENT_ID=4  
CACHET_ADMIN_API_COMPONENT_ID=5  
  
SHOPWARE_URL="http://shopware.local/"  
SHOPWARE_API_VERSION="v3"  
SHOPWARE_USER="admin"  
SHOPWARE_PW="shopware"  
SHOPWARE_STORE_API_TOKEN="XXXXXXXXXXXXXXXXXXXXXXXXXX"  
ALLOWED_ERROR_CODES=("200" "301")
TIMEOUT_CODES=("504" "408")

