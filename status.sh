#!/bin/bash

#######################
#### Cachet config ####
#######################
CACHET_API_VERSION="v1"
CACHET_URL="http://cachet.local/api/${CACHET_API_VERSION}/"
CACHET_KEY="XXXXXXXXXXXXX"

CACHET_RESPONSE_TIME_METRIC_ID=1
CACHET_SHOP_COMPONENT_ID=1
CACHET_STORE_API_COMPONENT_ID=4
CACHET_ADMIN_API_COMPONENT_ID=5

#######################
### Shopware config ###
#######################
SHOPWARE_URL="http://shopware.local/"
SHOPWARE_API_VERSION="v3"

SHOPWARE_USER="admin"
SHOPWARE_PW="shopware"

SHOPWARE_STORE_API_TOKEN="XXXXXXXXXXXXX"

ALLOWED_ERROR_CODES=("200" "301")
TIMEOUT_CODES=("504" "408")

##################
### Get status ###
##################
function cachet_status() {
    CACHET_STATUS=$(curl -LI --location "${CACHET_URL}ping" \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        -H "Cache-Control: no-cache" \
        -o /dev/null -w '%{http_code}\n' -s)
}

function shopware_status() {
    SHOPWARE_STATUS=$(curl "${SHOPWARE_URL}" \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        -H "Cache-Control: no-cache" \
        -o /dev/null -w '%{http_code}\n' -s)

    SHOPWARE_TIMING=$(curl -o /dev/null -s -w '%{time_total}\n' "${SHOPWARE_URL}" \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        -H "Cache-Control: no-cache")
}

function shopware_store_api_status() {
    SHOPWARE_STORE_API_STATUS=$(curl -L "${SHOPWARE_URL}store-api/${SHOPWARE_API_VERSION}/context/" \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        -H "Cache-Control: no-cache" \
        -H "sw-access-key: ${SHOPWARE_STORE_API_TOKEN}" \
        -o /dev/null -w '%{http_code}\n' -s)
}

function shopware_admin_api_status() {
    SHOPWARE_ADMIN_API_STATUS=$(curl -X POST -L "${SHOPWARE_URL}api/oauth/token" \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        -H "Cache-Control: no-cache" \
        -d "{
          \"client_id\": \"administration\",
          \"grant_type\": \"password\",
          \"scopes\": \"write\",
          \"username\": \"${SHOPWARE_USER}\",
          \"password\": \"${SHOPWARE_PW}\"
        }" \
        -o /dev/null -w '%{http_code}\n' -s)
}

###################
### Call cachet ###
###################

function post_response_metric() {
    RESPONSE_TIME=$1

    CACHET_METRIC_CALL=$(curl -X POST --location "${CACHET_URL}metrics/${CACHET_RESPONSE_TIME_METRIC_ID}/points" \
        -H "X-Cachet-Token: ${CACHET_KEY}" \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        -H "Cache-Control: no-cache" \
        -d "{
          \"value\": ${RESPONSE_TIME}
        }" \
        -o /dev/null -w '%{http_code}\n' -s)
}

function post_incident() {
    STATUS_CODE=$1
    COMPONENT_ID=$2
    ENDPOINT_NAME=$3
    COMPONENT_STATUS=$4

    SHOPWARE_ADMIN_API_STATUS=$(curl -X POST --location "http://cachet.local/api/v1/incidents" \
        -H "X-Cachet-Token: LWewabfqE6AqA2rl2Hzt" \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        -H "Cache-Control: no-cache" \
        -d "{
          \"component_id\": ${COMPONENT_ID},
          \"component_status\": ${COMPONENT_STATUS},
          \"name\": \"${ENDPOINT_NAME} not reachable\",
          \"status\": 2,
          \"visible\": 1,
          \"message\": \"${ENDPOINT_NAME} is not reachable due to error code ${STATUS_CODE} \"
        }" \
        -o /dev/null -w '%{http_code}\n' -s)
}

function put_component() {
    COMPONENT_ID=$1
    COMPONENT_STATUS=$2

    CACHET_PUT_STATUS=$(curl -X PUT --location "http://cachet.local/api/v1/components/${COMPONENT_ID}" \
        -H "X-Cachet-Token: LWewabfqE6AqA2rl2Hzt" \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        -H "Cache-Control: no-cache" \
        -d "{
          \"status\": ${COMPONENT_STATUS}
        }" \
        -o /dev/null -w '%{http_code}\n' -s)
}

##############
### Helper ###
##############

function check_status() {
    STATUS_CODE=$1
    COMPONENT_ID=$2
    ENDPOINT_NAME=$3

    if [[ " ${ALLOWED_ERROR_CODES[@]} " =~ " ${STATUS_CODE} " ]]; then
        if [ ${TYPE} == ${CACHET_SHOP_COMPONENT} ]; then
            post_response_metric $SHOPWARE_TIMING
        fi
        put_component $COMPONENT_ID 1
    else
        if [[ " ${TIMEOUT_CODES[@]} " =~ " ${STATUS_CODE} " ]]; then
            post_incident $STATUS_CODE $COMPONENT_ID $ENDPOINT_NAME 3
        else
            post_incident $STATUS_CODE $COMPONENT_ID $ENDPOINT_NAME 4
        fi
    fi

}

cachet_status
shopware_status
shopware_store_api_status
shopware_admin_api_status

# Replace comma with dot
SHOPWARE_TIMING=${SHOPWARE_TIMING/,/.}

check_status $SHOPWARE_STATUS $CACHET_SHOP_COMPONENT_ID "Shop"
check_status $SHOPWARE_STORE_API_STATUS $CACHET_STORE_API_COMPONENT_ID "StoreAPI"
check_status $SHOPWARE_ADMIN_API_STATUS $CACHET_ADMIN_API_COMPONENT_ID "AdminAPI"
